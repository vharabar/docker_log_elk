# HOW TO USE

### Use 
- stack_start to sart the stack
- stack_stop to stop the stack

### Kibana interface
available at [http://localhost:5601](http://localhost:5601)

It will ask to define default index: use `logstash-`
Time filter field name: use `@timestamp`

### To create a dashboard
- In discover:
  - In available fields select `docker.name` and `message` (click add on hover)
  - Click on save at the top right
- In Dashboard
  - Click on `add`
  - In sidebar click on Saved Search
  - Select the search you just saved 
- You're done!
- Don't forget to set auoupdate to 5 seconds